//
//  Int+isPrime_tests.swift
//  PrimeFinder
//
//  Created by Krzysztof Rutkowski on 24/01/2016.
//  Copyright © 2016 Krzysztof Rutkowski. All rights reserved.
//

@testable import PrimeFinder
import XCTest

class Int_isPrime_tests: XCTestCase
{
	// MARK: primes

	func test2() {
		XCTAssertTrue(2.isPrime())
	}

	func test3() {
		XCTAssertTrue(3.isPrime())
	}

	func test5() {
		XCTAssertTrue(5.isPrime())
	}

	func test997() {
		XCTAssertTrue(997.isPrime())
	}

	func test15485863() {
		XCTAssertTrue(15485863.isPrime())
	}

	func test15485867() {
		XCTAssertTrue(15485867.isPrime())
	}

	// MARK: composite

	func test15485865() {
		XCTAssertFalse(15485865.isPrime())
	}

	func test15485869() {
		XCTAssertFalse(15485869.isPrime())
	}

	// MARK: units

	func test1() {
		XCTAssertFalse(1.isPrime())
	}

	func test0() {
		XCTAssertFalse(0.isPrime())
	}
}
