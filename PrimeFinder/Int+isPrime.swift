//
//  Int+IsPrime.swift
//  PrimeFinder
//
//  Created by Krzysztof Rutkowski on 24/01/2016.
//  Copyright © 2016 Krzysztof Rutkowski. All rights reserved.
//

import UIKit

extension Int
{
	func isPrime() -> Bool {
		if self == 0 {
			return false
		} else if self == 1 {
			return false
		} else if self == 2 {
			return true
		} else if self == 3 {
			return true
		} else if self % 2 == 0 {
			return false
		} else if self % 3 == 0 {
			return false
		}

		var i = 5
		var w = 2

		while i * i <= self {
			if self % i == 0 {
				return false
			}

			i += w
			w = 6 - w
		}

		return true
	}
}
