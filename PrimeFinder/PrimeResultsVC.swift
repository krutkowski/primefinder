//
//  PrimeResultsVC.swift
//  PrimeFinder
//
//  Created by Krzysztof Rutkowski on 25/01/2016.
//  Copyright © 2016 Krzysztof Rutkowski. All rights reserved.
//

import UIKit

class PrimeResultsVC: UITableViewController {
	private lazy var primeFinder: PrimeFinder = {
		return PrimeFinder(callback: { (prime) -> () in
			self.processNextPrime(prime)
		})
	}()

	private lazy var numberFormatter: NSNumberFormatter = {
		let formatter = NSNumberFormatter()
		formatter.numberStyle = .DecimalStyle
		return formatter
	}()

	private var primes = [Int]()

	private func processNextPrime(prime: Int) {
		var scroll = false

		let indexPaths = tableView.indexPathsForVisibleRows
		if let indexPaths = indexPaths {
			let lastIndexPath = NSIndexPath(forRow: self.primes.count - 1, inSection: 0)
			if indexPaths.contains(lastIndexPath) {
				scroll = true
			}
		}

		self.primes.append(prime)
		let indexPath = NSIndexPath(forRow: self.primes.count - 1, inSection: 0)
		tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)

		if scroll {
			tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: .None, animated: true)
		}
	}

    override func viewDidLoad() {
        super.viewDidLoad()

		primeFinder.start()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return primes.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier("PrimeCell", forIndexPath: indexPath)

		let prime = primes[indexPath.row]
		cell.textLabel!.text = numberFormatter.stringFromNumber(prime)!

		return cell
	}
}
