//
//  PrimeFinder.swift
//  PrimeFinder
//
//  Created by Krzysztof Rutkowski on 24/01/2016.
//  Copyright © 2016 Krzysztof Rutkowski. All rights reserved.
//

import UIKit

public class PrimeFinder
{
	public typealias FoundPrime = (prime: Int) -> ()

	private var foundPrimeCallback: FoundPrime
	private var currentlyTestedNumbers = [Int]()
	private var nextToTest = 100000000000000000 //0
	public var numberOfConsecutitiveTests = 5

	public init(callback: FoundPrime) {
		foundPrimeCallback = callback
	}

	public func start() {
		testMaximumAtOnce()
	}

	private func testMaximumAtOnce() {
		while currentlyTestedNumbers.count < numberOfConsecutitiveTests {
			analyzeNext()
		}
	}

	private func analyzeNext() {
		let testedNumber = nextToTest
		nextToTest++
		currentlyTestedNumbers.append(testedNumber)

		let queue = dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)
		dispatch_async(queue) {

			let result = testedNumber.isPrime()

			dispatch_sync(dispatch_get_main_queue(), {
				let index = self.currentlyTestedNumbers.indexOf(testedNumber)!
				self.currentlyTestedNumbers.removeAtIndex(index)

				if result {
					self.foundPrimeCallback(prime: testedNumber)
				}

				self.testMaximumAtOnce()
			})

		}
	}
}
